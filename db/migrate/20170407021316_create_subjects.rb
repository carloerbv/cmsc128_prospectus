class CreateSubjects < ActiveRecord::Migration[5.0]
  def change
    create_table :subjects do |t|
      t.column :subject_title, :string, :limit => 32, :null => false
      t.column :subject_course, :string, :null => false
      t.column :units, :integer,:null => false
      t.column :subject_type, :string,:null => false
      t.column :year, :string,:null => false
      t.column :sem, :string,:null => false
    end
  end
end
