class CreateCourses < ActiveRecord::Migration[5.0]
  def change
    create_table :courses do |t|
     t.column :course_title, :string, :limit => 32, :null => false,:unique =>true
    end
  end
end
