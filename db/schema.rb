# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170407021316) do

  create_table "courses", force: :cascade do |t|
    t.string "course_title", limit: 32, null: false
  end

  create_table "subjects", force: :cascade do |t|
    t.string  "subject_title",  limit: 32, null: false
    t.string  "subject_course",            null: false
    t.integer "units",                     null: false
    t.string  "subject_type",              null: false
    t.string  "year",                      null: false
    t.string  "sem",                       null: false
  end

end
