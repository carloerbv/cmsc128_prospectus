Feature: Prospectus Management
  As a division admin
  I want to manage prospectus under my division
Scenario: Prospectus List
So that I could make changes that are appropriate
      Given I have courses titled Computer Science, Biology
      When I go to the prospectus page
      Then I should see "Computer Science"
      And I should see "Biology"
Scenario: Create prospectus
  Given I am on the prospectus page
  And I press "Add Prospectus"
  Then I should be on the add prospectus page
Scenario: Edit prospectus
  Given I am on the prospectus page
  And I press "Edit"
  Then I should be on the edit prospectus page
Scenario: Delete prospectus
  Given I am on the prospectus page
  And I press "Delete"
  Then I should see one less prospectus
