Feature: faculty signup
    As a faculty
    I want to be able to sign up for an account
    So that the I will be able to login into the app
Scenario: user does not have an account
    Given I am on the home page
    Then I should see "Welcome"
    When I press "Sign up"
    Then I should see "Create a faculty Account"
    And I fill in "Username:" with "Carl"
    And I fill in "Password:" with "password"
    And I fill in "Password Confirmation:" with "password"
    And I press "Signup"
    Then I should see "You signed up successfully"