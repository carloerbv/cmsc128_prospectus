Rails.application.routes.draw do
  get 'welcome/index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "welcome#index"

  resource :accounts
  match '/courses/new', to: 'courses#create', via: :post
  match '/courses/edit/:course_id', to: 'courses#edit', via: :get, :as => 'edit_course'
  match '/courses/edit', to: 'courses#edit', via: :post
  resource :courses
end
