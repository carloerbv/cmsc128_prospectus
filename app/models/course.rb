class Course < ApplicationRecord
  validates :course_title, :presence => true, :uniqueness => true, :length => { :in => 3..20 }
end
