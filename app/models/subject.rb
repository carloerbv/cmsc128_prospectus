class Subject < ApplicationRecord
  validates :subject_title, :presence => true, :uniqueness => true, :length => { :in => 3..20 }
  validates :subject_course, :presence => true, :length => { :in => 3..20 }
end
