class CoursesController < ApplicationController
  def show
    @courses=Course.all
  end
  def new
    @course=Course.new
  end
  def create
    @course=Course.create(course_params)
    if @course.save
      redirect_to :action => 'show'
    else
      render :action => 'new'
    end
  end
  def course_params
    params.fetch(:course, {}).permit(:course_title)
  end
  def edit
    @course=Course.find(params[:course_id])
  end
  def update
    @course=Course.find_by_id(params[:course_idt])
    if @course.update_attributes(course_params)
      redirect_to :action => 'show'
    else
      render :action => 'edit'
    end
  end
end
