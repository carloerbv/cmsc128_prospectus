class AccountsController < ApplicationController
    def index
        
    end
    
  def new
    @account = Account.new 
  end
  
  def create
    @account = Account.new(account_params)
    if @account.save
      flash[:notice] = "You signed up successfully"
      flash[:color]= "valid"
    else
      flash[:notice] = "Form is invalid"
      flash[:color]= "invalid"
    end
    
    render "new"
  end
  
  
    def account_params
      # It's mandatory to specify the nested attributes that should be whitelisted.
      # If you use `permit` with just the key that points to the nested attributes hash,
      # it will return an empty hash.
      params.require(:account).permit(:name, :password, :password_confirmation)
    end
end
